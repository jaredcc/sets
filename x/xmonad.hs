import XMonad
import XMonad.Util.EZConfig
import XMonad.Layout.NoBorders

main = xmonad $ settings

-- Main configuration, override the defaults to your liking.
                
settings = defaultConfig {
  layoutHook = smartBorders $ layoutHook defaultConfig,
  -- ^Smart borders hides border when there is only one window
  borderWidth = 2,
  terminal = "st",
  normalBorderColor  = "#5c5c5c",
  focusedBorderColor = "#FFD75F"
  } `additionalKeysP` keyConfig

keyConfig = 
  [ ("M-o", spawn "dmenu_run -p \">\" -fn \"Tamsyn-14\" -nb \"#212121\" -nf \"#9E9E9E\" -sb \"#424242\" -sf \"#FFD75F\"")
  ]
