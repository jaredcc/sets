# README.md
## Workmanjy -- Workman keyboard layout with  j  and  y  swapped
Swapped  j  and  y  to bring  j  to homerow for vi-binds; returns  y  to it's original qwerty position.

`$ sudo mkdir /usr/share/kbd/keymaps/i386/workmanjy`
`$ sudo cp workmanjy.map.gz /usr/share/kbd/keymaps/i386/workmanjy`
`$ sudo loadkeys workmanjy`

You can also add workmanjy to your `/etc/rc.conf` to add it to your system